<?php
include 'koneksi.php';
$querySoal = mysqli_query($conn, "SELECT * FROM soal_temp temp, soal sl
    WHERE temp.id_soal = sl.id_soal AND
    temp.id_mapel = '$_GET[idmapel]' AND 
    temp.id_user = '$_GET[iduser]' order by temp.no_soal asc");
$queryUser = mysqli_query($conn, "SELECT * FROM user WHERE id_user = '$_GET[iduser]'");
$queryMapel = mysqli_query($conn, "SELECT * FROM mapel mpl, start_ujian su, nilai nil
    WHERE mpl.id_mapel = su.id_mapel AND
     mpl.id_mapel = nil.id_mapel AND
     nil.id_user = '$_GET[iduser]' AND 
     nil.id_mapel = '$_GET[idmapel]'");
$dataUser = mysqli_fetch_array($queryUser);
$dataMapel = mysqli_fetch_array($queryMapel);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cetak Pengerjaan</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="css/styles.css">
</head>

<body onload="window.print()">
    <div class="container">
        <div class="row justify-content-center col-md-8">
            <div class="row col-md-8">
                <h4>Data Siswa</h4>
                <table class="table table-borderless">
                    <tr>
                        <th>Nama Siswa</th>
                        <td>:</td>
                        <td><?php echo $dataUser['nama']; ?></td>
                    </tr>
                    <tr>
                        <th>Mata pelajaran</th>
                        <td>:</td>
                        <td><?php echo $dataMapel['nm_mapel']; ?></td>
                    </tr>
                    <tr>
                        <th>Tanggal Ujian</th>
                        <td>:</td>
                        <td><?php echo $dataMapel['tgl_ujian']; ?></td>
                    </tr>
                    <tr>
                        <th>Waktu Ujian</th>
                        <td>:</td>
                        <td><?php echo $dataMapel['waktu_mulai']; ?></td>
                    </tr>
                    <tr>
                        <th>Nilai</th>
                        <td>:</td>
                        <td><?php echo $dataMapel['nilai']; ?></td>
                    </tr>
                </table>
            </div>
            <div class="row col-md-8">
                <ol class="listsoal">
                    <h4>Pengerjaan Soal</h4>
                    <?php
                    while ($data = mysqli_fetch_array($querySoal)) {
                    ?>
                        <li>
                            <?php echo $data['soal']; ?>
                            <?php if ($data['soal_tipe'] == '1') { ?>
                                <?php $rganswer = ""; ?>
                                <?php if ($data['benar_or_salah'] == '1') {
                                    $st = 'text-success';
                                } else {
                                    $st = 'text-danger';
                                    $rganswer = '<br><strong>Jawaban Benar : <span class="text-success">' . $data['answer'] . '</span></strong>';
                                } ?>
                                <ul class="lsesay">
                                    <li><strong>Jawaban Anda :
                                            <span class="<?php echo $st; ?>"><?php echo $data['jawaban_anda']; ?></span>
                                        </strong>
                                        <?php echo $rganswer; ?>
                                    </li>
                                </ul>
                            <?php } else { ?>
                                <?php $styl1 = "";
                                $styl2 = "";
                                $styl2 = "";
                                $styl3 = "";
                                $styl4 = ""; ?>
                                <?php if ($data['pilihan_1'] == $data['jawaban_anda'] && $data['benar_or_salah'] == '1') {
                                    $styl1 = "text-success";
                                }
                                if ($data['pilihan_1'] == $data['jawaban_anda'] && $data['benar_or_salah'] == '0') {
                                    $styl1 = "text-danger";
                                } ?>
                                <?php if ($data['pilihan_2'] == $data['jawaban_anda'] && $data['benar_or_salah'] == '1') {
                                    $styl2 = "text-success";
                                }
                                if ($data['pilihan_2'] == $data['jawaban_anda'] && $data['benar_or_salah'] == '0') {
                                    $styl2 = "text-danger";
                                } ?>
                                <?php if ($data['pilihan_3'] == $data['jawaban_anda'] && $data['benar_or_salah'] == '1') {
                                    $styl3 = "text-success";
                                }
                                if ($data['pilihan_3'] == $data['jawaban_anda'] && $data['benar_or_salah'] == '0') {
                                    $styl3 = "text-danger";
                                } ?>
                                <?php if ($data['pilihan_4'] == $data['jawaban_anda'] && $data['benar_or_salah'] == '1') {
                                    $styl4 = "text-success";
                                }
                                if ($data['pilihan_4'] == $data['jawaban_anda'] && $data['benar_or_salah'] == '0') {
                                    $styl4 = "text-danger";
                                } ?>
                                <ol class="lsabcd">
                                    <li class="<?php echo $styl1; ?>"><?php echo $data['pilihan_1']; ?></li>
                                    <li class="<?php echo $styl2; ?>"><?php echo $data['pilihan_2']; ?></li>
                                    <li class="<?php echo $styl3; ?>"><?php echo $data['pilihan_3']; ?></li>
                                    <li class="<?php echo $styl4; ?>"><?php echo $data['pilihan_4']; ?></li>
                                    <p><strong>Jawaban Benar : <span class="text-success"><?php echo $data['answer']; ?></span></strong></p>
                                </ol>
                            <?php } ?>
                        </li>
                    <?php
                    }
                    ?>
                </ol>
            </div>
        </div>
    </div>
</body>

</html>