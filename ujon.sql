-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 18, 2020 at 09:18 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ujon`
--

-- --------------------------------------------------------

--
-- Table structure for table `join_ujian`
--

CREATE TABLE `join_ujian` (
  `id_join` int(11) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `tgl_join` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `selesai_ujian` enum('0','1','','') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `join_ujian`
--

INSERT INTO `join_ujian` (`id_join`, `id_mapel`, `id_user`, `tgl_join`, `selesai_ujian`) VALUES
(4, 1, 8, '2020-05-18 04:57:36', '1'),
(5, 1, 7, '2020-05-18 06:01:49', '1');

-- --------------------------------------------------------

--
-- Table structure for table `mapel`
--

CREATE TABLE `mapel` (
  `id_mapel` int(11) NOT NULL,
  `nm_mapel` varchar(100) NOT NULL,
  `tgl_ujian` date NOT NULL,
  `waktu_ujian` time NOT NULL,
  `lama_ujian` varchar(11) NOT NULL,
  `token` varchar(5) NOT NULL,
  `id_user` int(11) NOT NULL,
  `bg` varchar(100) NOT NULL DEFAULT 'bgUjian.jpg'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mapel`
--

INSERT INTO `mapel` (`id_mapel`, `nm_mapel`, `tgl_ujian`, `waktu_ujian`, `lama_ujian`, `token`, `id_user`, `bg`) VALUES
(1, 'Seni Budaya & Olahraga', '2020-05-18', '10:34:00', '1800', 'ZOHkw', 1, 'bgUjian.jpg'),
(4, 'mapel 3', '2020-05-18', '10:56:00', '3600', 'v5suP', 1, 'bgUjian.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `nilai`
--

CREATE TABLE `nilai` (
  `id_nilai` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `nilai` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nilai`
--

INSERT INTO `nilai` (`id_nilai`, `id_user`, `id_mapel`, `nilai`) VALUES
(1, 8, 1, 88.89),
(2, 7, 1, 50);

-- --------------------------------------------------------

--
-- Table structure for table `soal`
--

CREATE TABLE `soal` (
  `id_soal` int(11) NOT NULL,
  `soal` text NOT NULL,
  `gambar_soal` varchar(255) DEFAULT NULL,
  `pilihan_1` text,
  `pilihan_2` text,
  `pilihan_3` text,
  `pilihan_4` text,
  `answer` text NOT NULL,
  `soal_tipe` enum('0','1','','') NOT NULL,
  `kd_mapel` int(11) NOT NULL,
  `tgl_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `soal`
--

INSERT INTO `soal` (`id_soal`, `soal`, `gambar_soal`, `pilihan_1`, `pilihan_2`, `pilihan_3`, `pilihan_4`, `answer`, `soal_tipe`, `kd_mapel`, `tgl_created`) VALUES
(1, 'Tari Kecak Berasal dari... ', NULL, 'Aceh', 'Bali', 'Jawa Timur', 'Jawa Tengah', 'Bali', '0', 1, '2020-05-17 12:14:24'),
(2, 'Klub bola dengan logo dibawah ini adalah... ', 'DC20200517191546.jpg', 'Chealsea', 'Arsenal', 'Barcelona', 'Real Madrib', 'Arsenal', '0', 1, '2020-05-17 12:15:44'),
(3, 'Tari Saman berasal dari... ', NULL, 'Aceh', 'Sumatra Utara', 'Sumatra Barat', 'Sumatra Selatan', 'Aceh', '0', 1, '2020-05-17 12:17:18'),
(4, 'Olahraga yang melelahkan adalah... ', NULL, '', '', '', '', 'berenang', '1', 1, '2020-05-17 12:18:12'),
(5, 'Reog ponorogo berasal dari', NULL, 'ponorogo', 'kediri', 'solo', 'papua', 'ponorogo', '0', 1, '2020-05-17 12:20:27'),
(6, 'Tokoh pewayangan nama lain dari werkudoro adalah', NULL, 'Arjuno', 'Bima', 'Yudistira', 'Nakula', 'Bima', '0', 1, '2020-05-18 01:42:18'),
(7, 'ibukota jatim', NULL, 'kediri', 'nganjyk', 'surabaya', 'malang', 'surabaya', '0', 1, '2020-05-18 04:01:48'),
(8, 'klub bola apa ini', 'DC20200518110253.jpg', '', '', '', '', 'spurs', '1', 1, '2020-05-18 04:02:53'),
(9, 'klub bola apa ini', 'DC20200518110254.jpg', '', '', '', '', 'spurs', '1', 1, '2020-05-18 04:02:54'),
(11, 'soal bergambar', 'DC20200518113119.jpg', '', '', '', '', 'sputs', '1', 4, '2020-05-18 04:31:19'),
(12, 'soal qiwi', NULL, 'pil 1', 'pil 2', 'pik 3', 'pik 4', 'pil 2', '0', 4, '2020-05-18 04:55:41'),
(15, 'soal bergambar 1244', 'DC20200518125937.jpg', '', '', '', '', 'ayam', '1', 1, '2020-05-18 05:59:37');

-- --------------------------------------------------------

--
-- Table structure for table `soal_temp`
--

CREATE TABLE `soal_temp` (
  `id_soal_temp` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `id_soal` int(11) NOT NULL,
  `jawaban_anda` text,
  `benar_or_salah` enum('0','1','','') DEFAULT NULL,
  `no_soal` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `soal_temp`
--

INSERT INTO `soal_temp` (`id_soal_temp`, `id_user`, `id_mapel`, `id_soal`, `jawaban_anda`, `benar_or_salah`, `no_soal`) VALUES
(19, 8, 1, 4, 'renang', '0', 1),
(20, 8, 1, 9, 'spurs', '1', 2),
(21, 8, 1, 2, 'Arsenal', '1', 3),
(22, 8, 1, 3, 'Aceh', '1', 4),
(23, 8, 1, 8, 'spurs', '1', 5),
(24, 8, 1, 5, 'ponorogo', '1', 6),
(25, 8, 1, 6, 'Bima', '1', 7),
(26, 8, 1, 7, 'surabaya', '1', 8),
(27, 8, 1, 1, 'Bali', '1', 9),
(28, 7, 1, 5, 'ponorogo', '1', 1),
(29, 7, 1, 3, 'Aceh', '1', 2),
(30, 7, 1, 1, 'Bali', '1', 3),
(31, 7, 1, 8, 'persik', '0', 4),
(32, 7, 1, 7, 'surabaya', '1', 5),
(33, 7, 1, 9, 'persija', '0', 6),
(34, 7, 1, 4, 'renang', '0', 7),
(35, 7, 1, 15, 'tes', '0', 8),
(36, 7, 1, 2, 'Arsenal', '1', 9),
(37, 7, 1, 6, NULL, NULL, 10);

-- --------------------------------------------------------

--
-- Table structure for table `start_ujian`
--

CREATE TABLE `start_ujian` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `tgl_ujian` date NOT NULL,
  `waktu_mulai` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `start_ujian`
--

INSERT INTO `start_ujian` (`id`, `id_user`, `id_mapel`, `tgl_ujian`, `waktu_mulai`) VALUES
(3, 8, 1, '2020-05-18', '11:58:14'),
(4, 7, 1, '2020-05-18', '13:02:09');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `tipe` enum('0','1','','') NOT NULL,
  `token` varchar(10) NOT NULL,
  `photos` varchar(255) NOT NULL DEFAULT 'default.jpg'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `nama`, `username`, `password`, `tipe`, `token`, `photos`) VALUES
(1, 'guru woke', 'guru1', '1234', '1', 'S2fzeqpP', 'DC_20200518114505.jpg'),
(7, 'siswa 1', 'siswa1', '1234', '0', 'KH1tWgPf', 'default.jpg'),
(8, 'siswa2', 'siswa2', '1234', '0', 'o2cMGg97', 'default.jpg'),
(9, 'nama123', 'nm1', '1234', '0', 'r1F2z9K9', 'DC_20200518125436.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `join_ujian`
--
ALTER TABLE `join_ujian`
  ADD PRIMARY KEY (`id_join`);

--
-- Indexes for table `mapel`
--
ALTER TABLE `mapel`
  ADD PRIMARY KEY (`id_mapel`),
  ADD UNIQUE KEY `token` (`token`);

--
-- Indexes for table `nilai`
--
ALTER TABLE `nilai`
  ADD PRIMARY KEY (`id_nilai`);

--
-- Indexes for table `soal`
--
ALTER TABLE `soal`
  ADD PRIMARY KEY (`id_soal`);

--
-- Indexes for table `soal_temp`
--
ALTER TABLE `soal_temp`
  ADD PRIMARY KEY (`id_soal_temp`);

--
-- Indexes for table `start_ujian`
--
ALTER TABLE `start_ujian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `join_ujian`
--
ALTER TABLE `join_ujian`
  MODIFY `id_join` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `mapel`
--
ALTER TABLE `mapel`
  MODIFY `id_mapel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `nilai`
--
ALTER TABLE `nilai`
  MODIFY `id_nilai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `soal`
--
ALTER TABLE `soal`
  MODIFY `id_soal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `soal_temp`
--
ALTER TABLE `soal_temp`
  MODIFY `id_soal_temp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `start_ujian`
--
ALTER TABLE `start_ujian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
