<?php

include 'koneksi.php';

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $respon = array(); 
    $respon['kode'] = '000';
    $mode = $_POST['mode'];
    if($mode == 'POST_DATA'){
        $soal = $_POST['soal'];
        $pilihan_1 = $_POST['pilihan_1'];
        $pilihan_2 = $_POST['pilihan_2'];
        $pilihan_3 = $_POST['pilihan_3'];
        $pilihan_4 = $_POST['pilihan_4'];
        $answer = $_POST['answer'];
        $tipe_soal = $_POST['soal_tipe'];
        $mapeel = $_POST['kd_mapel'];
        $imstr = $_POST['image'];
        $file = $_POST['file'];
        $path = "images/";

        $mpl = "SELECT * FROM mapel WHERE nm_mapel = '$mapeel'";
        $mplll = mysqli_query($conn,$mpl);
        $dtmpl = mysqli_fetch_assoc($mplll);
        $kd_mapel = $dtmpl['id_mapel'];
        if($imstr == ""){
            $sql = "INSERT INTO soal (soal,pilihan_1,pilihan_2,pilihan_3,pilihan_4,answer,soal_tipe,kd_mapel)
                VALUES ('$soal','$pilihan_1','$pilihan_2','$pilihan_3','$pilihan_4','$answer','$tipe_soal','$kd_mapel')";
        }else{
            if(file_put_contents($path.$file,base64_decode($imstr)) == false){
                $respon['kode'] = "111";
                echo json_encode($respon); exit();
            }else{
                $sql = "INSERT INTO soal (soal,gambar_soal,pilihan_1,pilihan_2,pilihan_3,pilihan_4,answer,soal_tipe,kd_mapel)
                VALUES ('$soal','$file','$pilihan_1','$pilihan_2','$pilihan_3','$pilihan_4','$answer','$tipe_soal','$kd_mapel')";
            }
        }
    }
    if($mode == 'UPDATE_DATA'){
        $soal = $_POST['soal'];
        $pilihan_1 = $_POST['pilihan_1'];
        $pilihan_2 = $_POST['pilihan_2'];
        $pilihan_3 = $_POST['pilihan_3'];
        $pilihan_4 = $_POST['pilihan_4'];
        $answer = $_POST['answer'];
        $tipe_soal = $_POST['soal_tipe'];
        $mapeel = $_POST['kd_mapel'];
        $imstr = $_POST['image'];
        $file = $_POST['file'];
        $path = "images/";

        $mpl = "SELECT * FROM mapel WHERE nm_mapel = '$mapeel'";
        $mplll = mysqli_query($conn,$mpl);
        $dtmpl = mysqli_fetch_assoc($mplll);
        $kd_mapel = $dtmpl['id_mapel'];
        $id_soal = $_POST['id_soal'];
        if($imstr == ""){
            $sql = "UPDATE soal SET soal = '$soal', pilihan_1 = '$pilihan_1', pilihan_2 = '$pilihan_2', pilihan_3 = '$pilihan_3', 
                pilihan_4 = '$pilihan_4', answer = '$answer', soal_tipe = '$tipe_soal', kd_mapel = '$kd_mapel' 
                WHERE id_soal = '$id_soal'";
        }else{
            if(file_put_contents($path.$file,base64_decode($imstr)) == false){
                $respon['kode'] = "111";
                echo json_encode($respon); exit();
            }else{
                $sql = "UPDATE soal SET soal = '$soal', gambar_soal = '$file', pilihan_1 = '$pilihan_1', pilihan_2 = '$pilihan_2', pilihan_3 = '$pilihan_3', 
                pilihan_4 = '$pilihan_4', answer = '$answer', soal_tipe = '$tipe_soal', kd_mapel = '$kd_mapel' 
                WHERE id_soal = '$id_soal'";
            }
        }
    }
    if($mode == 'DELETE_DATA'){
        $id_soal = $_POST['id_soal'];
        $sql = "DELETE FROM soal WHERE id_soal = '$id_soal'";
    }
    $result = mysqli_query($conn,$sql);
    if($result){
        echo json_encode($respon); 
    }else{
        $respon['kode'] = "111";
        echo json_encode($respon);
    }
 }